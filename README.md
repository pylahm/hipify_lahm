# Hipify_lahm

hipify_lahm is a python utility to convert CUDA C/C++ code into HIP C/C++ code.
It is NOT a parser; it does a smart but basic search-and-replace based on CUDA-to-HIP mappings which are specified in the hipify_lahm module.
It can also "hipify" the header include statements in your source code to ensure that it's the hipified header files that are included.
